Option Strict Off
Option Explicit On
Imports System.IO.Ports
Imports System.Net
Imports MySql.Data.MySqlClient
Imports System.Net.HttpWebRequest
Imports System.Web
Imports System.IO
Imports Newtonsoft.Json
Imports System.Collections.Generic

Friend Class Form1

#Region "Declaracion de Variables"
    Inherits System.Windows.Forms.Form
    'Declaration of Private Subroutine
    Private Declare Sub openport Lib "tsclib.dll" (ByVal PrinterName As String)
    Private Declare Sub closeport Lib "tsclib.dll" ()
    Private Declare Sub sendcommand Lib "tsclib.dll" (ByVal command_Renamed As String)
    Private Declare Sub setup Lib "tsclib.dll" (ByVal LabelWidth As String, ByVal LabelHeight As String, ByVal Speed As String, ByVal Density As String, ByVal Sensor As String, ByVal Vertical As String, ByVal Offset As String)
    Private Declare Sub downloadpcx Lib "tsclib.dll" (ByVal Filename As String, ByVal ImageName As String)
    Private Declare Sub barcode Lib "tsclib.dll" (ByVal X As String, ByVal Y As String, ByVal CodeType As String, ByVal Height_Renamed As String, ByVal Readable As String, ByVal rotation As String, ByVal Narrow As String, ByVal Wide As String, ByVal Code As String)
    Private Declare Sub printerfont Lib "tsclib.dll" (ByVal X As String, ByVal Y As String, ByVal FontName As String, ByVal rotation As String, ByVal Xmul As String, ByVal Ymul As String, ByVal Content As String)
    Private Declare Sub clearbuffer Lib "tsclib.dll" ()
    Private Declare Sub printlabel Lib "tsclib.dll" (ByVal NumberOfSet As String, ByVal NumberOfCopy As String)
    Private Declare Sub formfeed Lib "tsclib.dll" ()
    Private Declare Sub nobackfeed Lib "tsclib.dll" ()
    Private Declare Sub windowsfont Lib "tsclib.dll" (ByVal X As Short, ByVal Y As Short, ByVal fontheight_Renamed As Short, ByVal rotation As Short, ByVal fontstyle As Short, ByVal fontunderline As Short, ByVal FaceName As String, ByVal TextContent As String)

    Dim Rx As String
    Dim Letak As String
    Dim Timangan As String
    Dim valor As String
    Public Event DataReceived As IO.Ports.SerialDataReceivedEventHandler
    Private MysqlCnn As New MySqlConnection
    Private adaptador As MySqlDataAdapter
    Private datos As DataSet
    Dim puerto As New SerialPort
    Dim datos2 As String
#End Region

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LeerPuertoBalanza()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try

            Dim NumeroOrden As String = txtNumeroOrden.Text()
            Dim codigoBarra As String = getCodigoBarra()
            Dim codigoProducto As String = getCodigoProducto()
            Dim descripcion As String = getDescripcion()
            Dim fecha As String = DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToShortTimeString()
            Dim peso As String = lblPeso.Text
            Dim logo As String = "ENVASES"
            Dim logo2 As String = "SAN NICOLAS SAC"
            Dim encargado As String = txtEncargado.Text()


            If txtNumeroOrden.Text <> "" And
                txtEncargado.Text <> "" And
                cmbCodigo.SelectedItem.ToString() <> "" And
                lblPeso.Text > 0 Then

                Call openport("TSC TTP-244 Plus")
                Call setup("108", "70", "4.0", "7", "0", "5", "1")
                Call clearbuffer()
                Call sendcommand("DIRECTION 1")
                Call sendcommand("GAP 3 mm,0 mm")
                'Call sendcommand("FORMFEED")

                'Call sendcommand("SET CUTTER OFF")
                Call windowsfont(50, 270, 30, 0, 0, 0, "ARIAL", NumeroOrden)
                'Call windowsfont(450, 270, 30, 0, 0, 0, "ARIAL", "COD. PRODUCTO")
                'Call windowsfont(450, 300, 30, 0, 0, 0, "ARIAL", "COD. PRODUCTO" + codigoProducto)
                Call windowsfont(50, 280, 30, 0, 0, 0, "ARIAL", "COD. PRODUCTO: " + codigoProducto)
                Call windowsfont(50, 320, 30, 0, 0, 0, "ARIAL", "DESCRIP. PRODUCTO:")
                Call windowsfont(50, 355, 30, 0, 0, 0, "ARIAL", descripcion)
                Call windowsfont(50, 390, 30, 0, 0, 0, "ARIAL", peso)
                Call windowsfont(450, 400, 30, 0, 0, 0, "ARIAL", "ENCARGADO")
                Call windowsfont(450, 450, 30, 0, 0, 0, "ARIAL", encargado)
                Call windowsfont(50, 455, 30, 0, 0, 0, "ARIAL", fecha)
                Call windowsfont(50, 190, 40, 0, 2, 0, "ARIAL", logo)
                Call windowsfont(50, 220, 40, 0, 2, 0, "ARIAL", logo2)
                Call barcode(480, 190, "128", "80", "1", "0", "4", "4", codigoBarra)
                '             X , Y  , CODE, Heig, human, rotation, narrow, wide, aling, content   
                'LINEAS
                Call sendcommand("BOX 20,150,830,500,4")

                Call printlabel("1", "1")

                Call closeport()

                MsgBox("Imprimiendo...", vbInformation)
                txtNumeroOrden.Text = ""
                cmbCodigo.Items.Clear()
                cmbCodigo.Items.Add("")
                cmbCodigo.SelectedIndex = 0
                pBar1.Visible = False
                pBar1.Value = 1
            Else
                MsgBox("Rellene Todos Los Campos", vbInformation)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            MysqlCnn.Close()
        End Try
    End Sub

    Private Function getCodigoBarra()
        Dim list = cmbCodigo.SelectedItem
        Dim a As String = list.id_pedido.ToString() + "-" + list.codigo_producto.ToString()
        Return a
    End Function

    Private Function getCodigoProducto()
        Dim list = cmbCodigo.SelectedItem
        Return list.codigo_producto
    End Function

    Private Function getDescripcion()
        Dim list = cmbCodigo.SelectedItem
        Return list.descripcion
    End Function



    Private Sub txtNumeroOrden_KeyDown(sender As Object, e As KeyEventArgs) Handles txtNumeroOrden.KeyDown
        Try
            If e.KeyCode = Keys.Enter Then
                pBar1.Visible = True
                pBar1.Minimum = 1

                Dim listado = GetRequestURL("http://192.168.1.121/produccion/api/filtrar/pedido?filtro={0}", txtNumeroOrden.Text)

                pBar1.Maximum = listado.ToArray().Length
                pBar1.Value = 1
                pBar1.Step = 10

                cmbCodigo.Items.Clear()
                cmbCodigo.Items.AddRange(listado.ToArray())
                cmbCodigo.ValueMember = "id_pedido"
                cmbCodigo.DisplayMember = "descripcion"
                cmbCodigo.SelectedIndex = 0
                pBar1.PerformStep()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Function GetRequestURL(url As String, filtro As String)
        Dim workspace
        Dim request As WebRequest = WebRequest.Create(String.Format(url, filtro))
        request.Method = "GET"
        Dim response As WebResponse = request.GetResponse()
        Dim inputstream1 As Stream = response.GetResponseStream()
        Dim reader As New StreamReader(inputstream1)
        workspace = reader.ReadToEnd

        Dim dic = JsonConvert.DeserializeObject(Of List(Of ProductoPedido))(workspace)


        inputstream1.Dispose()
        reader.Close()
        response.Close()

        Return dic
    End Function

    Private Sub txtNumeroOrden_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumeroOrden.KeyPress
        If Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub


    Private Sub txtEncargado_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtEncargado.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsSeparator(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub


#Region "BALANZA"
    Dim inputData As String = ""
    Private Sub LeerPuertoBalanza()
        Try
            Puerto2.PortName = "COM10"
            Puerto2.BaudRate = 9600
            Puerto2.Parity = Parity.None
            Puerto2.StopBits = StopBits.One
            Puerto2.DataBits = 8
            Puerto2.Handshake = Handshake.None
            Puerto2.RtsEnable = True
            Puerto2.ReadBufferSize = 1024

            If Puerto2.IsOpen Then
                Puerto2.Close()
                Puerto2.Open()
            Else
                Puerto2.Open()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub


    Dim Remplazo As String
    Private Sub Puerto2_DataReceived(sender As Object, e As SerialDataReceivedEventArgs) Handles Puerto2.DataReceived
        Dim sp As SerialPort = CType(sender, SerialPort)
        datos2 = sp.ReadLine.Replace("ST,GS", "")
        Console.WriteLine("Data Received: " + datos2.Replace("US,GS", ""))
        Me.Invoke(New EventHandler(AddressOf DoUpdate))
    End Sub

    Private Sub DoUpdate(sender As Object, e As EventArgs)
        lblPeso.Text = datos2.Replace("US,GS", "")
    End Sub



#End Region


End Class

